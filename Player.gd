extends RigidBody

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)
	#Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	print(InputMap.get_actions())
	pass
var rotationDamp = true
var fuel = 100.0
var drainrate = 10
var refuelrate = 5
var MOUSE_SENSITIVITY_SCROLL_WHEEL = 0.08
var linearDamp = false
func _input(event):
	# Called upon user input.
	if (event is InputEventKey): 
		if (event.scancode == KEY_ESCAPE):
			get_tree().quit()
		if (event.scancode == KEY_SHIFT):
			linearDamp = event.pressed
		if (event.scancode == KEY_CONTROL and event.pressed):
			rotationDamp = !rotationDamp
	if event is InputEventMouseButton:
        if event.button_index == BUTTON_WHEEL_UP:
            refuelrate += MOUSE_SENSITIVITY_SCROLL_WHEEL
        elif event.button_index == BUTTON_WHEEL_DOWN:
            refuelrate -= MOUSE_SENSITIVITY_SCROLL_WHEEL
	pass

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	fuel += delta * refuelrate
	find_node("Fuel").value = fuel
	pass
func _integrate_forces(state):
	var modifier = 10;
	var start_fuel = fuel
	# Makes angular velocity decrease over time (We can make up some canon reason for this I guess)
	# Really this is here to make controlling the ship not literally hell
	if rotationDamp:
		state.angular_velocity *= pow(0.2, state.step)
	if (fuel > 0):
		if linearDamp:
			var temp = state.linear_velocity.length()
			state.linear_velocity *= pow(0.2, state.step)
			fuel -= state.step * drainrate * (temp-state.linear_velocity.length()) * 2
		if Input.is_key_pressed(KEY_W):
			state.apply_impulse(Vector3(0,0,0), transform.basis.z * modifier)
			fuel -= state.step * drainrate
		if Input.is_key_pressed(KEY_S):
			state.apply_impulse(Vector3(0,0,0), -transform.basis.z * modifier)
			fuel -= state.step * drainrate
		# These almost made control *intuitive*. Can't have that!
#		if Input.is_key_pressed(KEY_A):
#			state.apply_impulse(Vector3(0,0,0), transform.basis.x * modifier)
#		if Input.is_key_pressed(KEY_D):
#			state.apply_impulse(Vector3(0,0,0), -transform.basis.x * modifier)
		if Input.is_key_pressed(KEY_UP) or Input.is_key_pressed(KEY_I):
			fuel -= state.step * drainrate
			state.apply_torque_impulse(transform.basis.x * modifier)
		if Input.is_key_pressed(KEY_DOWN) or Input.is_key_pressed(KEY_K):
			fuel -= state.step * drainrate
			state.apply_torque_impulse(-transform.basis.x * modifier)
		# I'll trick Robert into debugging this...
		if Input.is_key_pressed(KEY_LEFT) or Input.is_key_pressed(KEY_A) or Input.is_key_pressed(KEY_J):
			fuel -= state.step * drainrate
			state.apply_torque_impulse(transform.basis.y * modifier)
		if Input.is_key_pressed(KEY_RIGHT) or Input.is_key_pressed(KEY_D) or Input.is_key_pressed(KEY_L):
			fuel -= state.step * drainrate
			state.apply_torque_impulse(-transform.basis.y * modifier)
		if Input.is_key_pressed(KEY_U):
			fuel -= state.step * drainrate
			state.apply_torque_impulse(-transform.basis.z * modifier)
		if Input.is_key_pressed(KEY_O):
			fuel -= state.step * drainrate
			state.apply_torque_impulse(transform.basis.z * modifier)
	find_node("Fuel").value = fuel
	find_node("Drain").value = (start_fuel - fuel) / state.get_step()
	pass