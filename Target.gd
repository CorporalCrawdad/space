extends RigidBody

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass

func _integrate_forces(state):
	# Called every physics update.
	# Update object motion here.
	var target_position = get_node("../Ship").get_global_transform()
	look_follow(state, get_global_transform(), target_position)
	pass

func look_follow(state, current_transform, target_transform):
	var target_position = target_transform.origin
	var cur_dir = current_transform.basis.z
	var target_dir = (target_position - current_transform.origin).normalized()
	var up_dir = cur_dir.cross(target_dir)
	var rotation_angle = acos(cur_dir.x) - acos(target_dir.x)
	rotation_angle = cur_dir.angle_to(target_dir)
	
	state.set_angular_velocity(up_dir * (rotation_angle / state.get_step()))
